package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.NguoidungDto;
import com.example.demo.model.NguoiDungEntity;
import com.example.demo.repository.NguoiDungRepository;

@RestController
@RequestMapping("/api")
public class NguoiDungController {
	@Autowired
    private NguoiDungRepository nguoiDungRepository;
	
	@GetMapping("/nguoidung")
    public List<NguoiDungEntity> getAllNguoiDung() {
        return nguoiDungRepository.findAll();
    }
    
    @RequestMapping(value = "/nguoidung", headers = "Accept=application/json", method = RequestMethod.POST)
    public NguoiDungEntity createNguoiDung(@RequestBody NguoidungDto nguoiDung) {
    	//return nguoiDung;
        return nguoiDungRepository.save(nguoiDung.toEntity());
    }
    
    @RequestMapping(value = "/test", headers = "Accept=application/json", method = RequestMethod.POST)
    public NguoidungDto test(@RequestHeader(name = "content-type") String contentType, @RequestBody NguoidungDto nguoiDung) {
    	return new NguoidungDto();
       // return nguoiDungRepository.save(nguoiDung);.
    }
    
    
}
