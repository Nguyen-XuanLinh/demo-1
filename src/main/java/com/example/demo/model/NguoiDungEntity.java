package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NguoiDung")
public class NguoiDungEntity {

	private Integer id;
	private String TenNguoiDung;
	private String TaiKhoan;
	private String MatKhau;
	private Date NgayTao;
	private String SoDienThoai;
	private String Gmai;
	private Integer VaiTro;
	
	public NguoiDungEntity(Integer id, String tenNguoiDung, String taiKhoan, String matKhau, Date ngayTao, String soDienThoai, String gmai, Integer vaiTro) {
        this.id = id;
        TenNguoiDung = tenNguoiDung;
        TaiKhoan = taiKhoan;
        MatKhau = matKhau;
        NgayTao = ngayTao;
        SoDienThoai = soDienThoai;
        Gmai = gmai;
        VaiTro = vaiTro;
    }

    public NguoiDungEntity() {
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    
    @Column(name = "TenNguoiDung", nullable = false)
    public String getTenNguoiDung() {
        return TenNguoiDung;
    }

    public void setTenNguoiDung(String tenNguoiDung) {
        TenNguoiDung = tenNguoiDung;
    }

    @Column(name = "TaiKhoan", nullable = false)
    public String getTaiKhoan() {
        return TaiKhoan;
    }

    public void setTaiKhoan(String taiKhoan) {
        TaiKhoan = taiKhoan;
    }

    @Column(name = "MatKhau", nullable = false)
    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String matKhau) {
        MatKhau = matKhau;
    }

    @Column(name = "NgayTao", nullable = false)
    public Date getNgayTao() {
        return NgayTao;
    }

    public void setNgayTao(Date ngayTao) {
        NgayTao = ngayTao;
    }

    @Column(name = "SoDienThoai", nullable = false)
    public String getSoDienThoai() {
        return SoDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        SoDienThoai = soDienThoai;
    }

    @Column(name = "Gmail", nullable = false)
    public String getGmai() {
        return Gmai;
    }

    public void setGmai(String gmai) {
        Gmai = gmai;
    }

    @Column(name = "VaiTro", nullable = false)
    public Integer getVaiTro() {
        return VaiTro;
    }

    public void setVaiTro(Integer vaiTro) {
        VaiTro = vaiTro;
    }
   
}
