package com.example.demo.model;

import java.sql.Date;

public class NguoidungDto {
	public Integer id;
	public String tenNguoiDung;
	public String taiKhoan;
	public String matKhau;
	public Date ngayTao;
	public String soDienThoai;
	public String gmai;
	public Integer vaiTro;
	
	public NguoiDungEntity toEntity() {
		return new NguoiDungEntity(id, tenNguoiDung, taiKhoan, matKhau, ngayTao, soDienThoai, gmai, vaiTro);
	}
}
